function [] = saveNFO(F, Achar, Bchar, mode, gapScr, AlignmentA, AlignmentB, matches, gaps, ...
            matchScrAA, matchScrTT, matchScrGG, matchScrCC, mismatchScrAT_TA, mismatchScrAC_CA, mismatchScrAG_GA, ...
            mismatchScrGC_CG, mismatchScrGT_TG, mismatchScrTC_CT, ...
            xAxisTitle, yAxisTitle, minimum, maximum)
    
    % saveNFO(); responsible for: creating the alignment process data info
    %            table, and allowing user to save it into the *.txt/*.xls file;
    
    ln1 = string("# seq 1: " + Achar);                       % seq1
    ln2 = string("# seq 1 - label: " + xAxisTitle);            % seq1 label
    ln3 = string("# seq 2: " + Bchar);                       % seq2
    ln4 = string("# seq 2 - label: " + yAxisTitle);             % seq2 label
    ln5 = "# Mode: " + mode;                             % mode
    ln6 = "# Match AA: " + matchScrAA;                   % match score for A-A lineup etc.
    ln7 = "# Match TT: " + matchScrTT;                   %
    ln8 = "# Match GG: " + matchScrGG;                   %
    ln9 = "# Match CC: " + matchScrCC;                   %
    ln10 = "# Mismatch AT/TA: " + mismatchScrAT_TA;      % mismatch score for A-T/T-A lineup etc.
    ln11 = "# Mismatch AC/CA: " + mismatchScrAC_CA;      %
    ln12 = "# Mismatch AG/GA: " + mismatchScrAG_GA;      %
    ln13 = "# Mismatch GC/CG: " + mismatchScrGC_CG;      %
    ln14 = "# Mismatch GT/TG: " + mismatchScrGT_TG;      %
    ln15 = "# Mismatch TC/CT: " + mismatchScrTC_CT;      %
    ln16 = "# Gap: " + gapScr;                           % gap score
    
    if mode == "similarity"
        ln17 = "# Maximum score: " + maximum;            % MAX/MIN score depending on chosen backtrace strategy;
    elseif mode == "distance"
        ln17 = "# Minimum score: " + minimum;  
    end
    
    ln18 = "# Length: " + length(char(AlignmentA));  % total length
    
    idenityPercent = (matches/length(char(AlignmentA))) * 100;
    ln19 = "# Identity: " + matches + "/" + length(char(AlignmentA)) + " (" + idenityPercent + "%)";   % matches/length [%]
    
    gapsPercent = (gaps/length(char(AlignmentA))) * 100;
    ln20 = "# Gaps: " + gaps + "/" + length(char(AlignmentA)) + " (" + gapsPercent + "%)";            % gaps/length [%]
   
    ln21 = AlignmentA;                              % A alignment sequence
    
    spaces = "";                                    % match/mismatch/gap string init;
    charAlignmentA = (char(AlignmentA));
    charAlignmentB = (char(AlignmentB));
    
    for i=1:1:length(char(AlignmentA))              
    spaces = spaces + " ";
    end
    
    charSpaces = char(spaces);
    
    for i=1:1:length(char(AlignmentA))              % set '|' where match occurs, set 'X' where mismatch/gap occurs
        if charAlignmentA(1, i) == charAlignmentB(1, i)
            charSpaces(1, i) = "|";
        else
            charSpaces(1, i) = "X";
        end
    end
    
    ln22 = string(charSpaces);    % similarity String assign;
    ln23 = AlignmentB;            % B alignment sequence;
    % ---------------------------------------------------------------------
    
    % create dataTable with beforehand assigned lines (ln1 -> ln13);
    dataTable = table(ln1, ln2, ln3, ln4, ln5, ln6, ln7, ln8, ln9, ln10, ...
    ln11, ln12, ln13, ln14, ln15, ln16, ln17, ln18, ln19, ln20, ln21, ln22, ln23);
    dataTable = table2cell(dataTable);
    dataTable = cell2table(dataTable'); % final String data in columns ready to be generated;
    
    % save file dialog box;
    filter = {'*.txt';'*.xls'}; % available file extensions;
    [filename, pathname] = uiputfile(filter, 'Save alignment analysis NFO'); % box set;
    fullpath = string(pathname) + string(filename);
    
    if string(filename)==""
        writetable(dataTable, fullpath, 'WriteVariableNames',0);  % save file if filename was provided;
    end
    
 end