# seq 1: _ATCCGATCGCGATAGCT
# seq 1 - label: 1.(A) user input sequence
# seq 2: _CGAGGACCGA
# seq 2 - label: 2.(B) user input sequence
# Mode: similarity
# Match AA: 2
# Match TT: 5
# Match GG: 4
# Match CC: 5
# Mismatch AT/TA: -0.5
# Mismatch AC/CA: -0.5
# Mismatch AG/GA: -0.5
# Mismatch GC/CG: -0.5
# Mismatch GT/TG: -0.5
# Mismatch TC/CT: -0.5
# Gap: -1
# Maximum score: 23.5
# Length: 11
# Identity: 7/11 (63.6364%)
# Gaps: 3/11 (27.2727%)
CGA__TCGCGA
|||XXX|X|||
CGAGGAC_CGA
