function [a, b, F, Achar, Bchar, gapScr, mode, ...
            matchScrAA, matchScrTT, matchScrGG, matchScrCC, mismatchScrAT_TA, mismatchScrAC_CA, mismatchScrAG_GA, ...
            mismatchScrGC_CG, mismatchScrGT_TG, mismatchScrTC_CT] = generateMatrix(A, B, gapScr, ...
            matchScrAA, matchScrTT, matchScrGG, matchScrCC, mismatchScrAT_TA, mismatchScrAC_CA, mismatchScrAG_GA, ...
            mismatchScrGC_CG, mismatchScrGT_TG, mismatchScrTC_CT)

    % generate Matrix: to pass: a, b, F, Achar, Bchar, matchScr, mismatchScr, gapScr, mode
    % generateMatrix(A,B); function responsible for generating the base
    %                      matrix passed to 'alignment.m' func.;

    %A = cellstr('ATCCGATCGCGATAGCT'); %#script testing value
    %B = cellstr('CGAGGACCGA');        %#script testing value

    mode = "";              % NFO DATA - initialize mode String (for afterward 'distance' / 'simiatirty' assign)
    dash = '_';             
    A = strcat(dash, A);
    B = strcat(dash, B);    % adding a init 'gap' at the beginning of both sequences;

    a = length(char(A));
    b = length(char(B));
    
    % planeSize as a matrix containing the zerosPlane size n+1(gap) x m+1(gap); 
    planeSize = [a, b];
    % crucial zerosPlane matrix assign (stores scoring values);
    zerosPlane = zeros(planeSize);

    % algorithm

    F = zerosPlane;
    
    %                           SCRIPT TESTING VALUES:
    %     matchScr= 2;                  
    %     matchScrAA = 2;
    %     matchScrTT = 3;
    %     matchScrGG = 4;
    %     matchScrCC = 5;
    %     mismatchScr = -0.5;
    %     mismatchScrAT_TA = -0.5;
    %     mismatchScrAC_CA = -0.5;
    %     mismatchScrAG_GA = -0.5;
    %     mismatchScrGC_CG = -0.5;
    %     mismatchScrGT_TG = -0.5;
    %     mismatchScrTC_CT = -0.5;
    %     gapScr = -1;
    
    % ---------------------------------------------------------------------


    for i=2:1:(a)           % fill the score vertical gap column no.1;
      F(i,1) = 0;
    end

    for i=2:1:(b)           % fill the score horizontal gap column no.1;
      F(1,i) = 0;
    end

    Achar = char(A);
    Bchar = char(B);

    for i=2:1:(a)

        for j=2:1:(b)

            mismatchScr = currentMismatchScrAssign(i, j, Achar, Bchar,  mismatchScrAT_TA, ...
mismatchScrAC_CA, mismatchScrAG_GA, mismatchScrGC_CG, mismatchScrGT_TG, mismatchScrTC_CT);       % mismatchScr variable assignment based on the current pair of nucleotide being compared;
                                                                                                 %
            matchScr = currentMatchScrAssign(i, j, Achar, Bchar, ...                             %
            matchScrAA, matchScrCC, matchScrGG, matchScrTT);                                     % matchScr -||-
                                                            
            if Achar(i)==Bchar(j)
                currentMatch = (F(i-1,j-1) + matchScr);     % assign equivalent score diagonally if the corresponding sequence nucleotides match;
            else                                            %
                currentMatch = (F(i-1,j-1) + mismatchScr);  % -||- MISmatch;
            end                                             %
                                                            %
            currentGapLeft = (F(i-1,j) + gapScr);           % -||- horizontally for possible gap (INSERTION);
            currentGapUp = (F(i,j-1) + gapScr);             % -||- vertically for possible gap (DELETION);

            if matchScr >= mismatchScr && matchScr >= gapScr                % if the 'matching score' from all of the input scoring values is the highest one, optimal path finding will base on minimalization (going down to '0')- set the mode for 'similarity';
                current = max([currentMatch currentGapLeft currentGapUp 0]);% assign the MIN score;
                mode = "similarity";     
                    if current<0
                        current=0;      % if similarity strategy is set and the 
                                        % current nucleotide lineup score value is BELOW '0' set it to '0'; 
                    end
            elseif matchScr <= mismatchScr && matchScr <= gapScr            % -||- is the lowest -||- optimal path finding will base on maximization (going up to '0'), mode -> 'distance';
                current = min([currentMatch currentGapLeft currentGapUp 0]);% assign the MAX score;
                    if current>0
                            current=0;  % if 'distance' strategy is set and the 
                                        % current nucleotide lineup score value is ABOVE '0' set it to '0'; 
                    end
                mode="distance";
            end
            
            F(i,j) = current;  % set current (MIN/MAX) score for current position in the (i,j) F zeroes matrix ;

        end

    end

    % ---------------------------------------------------------------------

end
