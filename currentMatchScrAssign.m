function [matchScr] = currentMatchScrAssign(i, j, Achar, Bchar, matchScrAA, matchScrCC, matchScrGG, matchScrTT)
    
    % currentMatchScrAssign() func. is responsible for currently analyzed 
    % nucleotide lineup MATCH score assignment inside the main algorith loop in the alignment.m function 
    % (match score set-up);

    matchScr = 0;
    
    if Achar(i)=='A' & Bchar(j)=='A'
                    matchScr = matchScrAA;
                elseif Achar(i)=='T' & Bchar(j)=='T'
                    matchScr = matchScrTT;
                elseif Achar(i)=='G' & Bchar(j)=='G'
                    matchScr = matchScrGG;
                elseif Achar(i)=='C' & Bchar(j)=='C'
                    matchScr = matchScrCC;
     end
    
end