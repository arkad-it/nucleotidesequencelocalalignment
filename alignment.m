function [F, Achar, Bchar, gapScr, AlignmentA, AlignmentB, matches, gaps, route, ...
            matchScrAA, matchScrTT, matchScrGG, matchScrCC, mismatchScrAT_TA, mismatchScrAC_CA, mismatchScrAG_GA, ...
            mismatchScrGC_CG, mismatchScrGT_TG, mismatchScrTC_CT, minimum, maximum] = alignment(a, b, F, Achar, Bchar, mode, gapScr, ...
            matchScrAA, matchScrTT, matchScrGG, matchScrCC, mismatchScrAT_TA, mismatchScrAC_CA, mismatchScrAG_GA, ...
            mismatchScrGC_CG, mismatchScrGT_TG, mismatchScrTC_CT)

    % alignment(); func. responsible for finding the optimal route of
    % both sequences alignment;
    % -> creates a '0's and '1's matrix with the apppropriate cells marked
    %    as the optimal route/apath;
    % -> counts NFO DATA variables;

    AlignmentA = "";            % alignment A seq init;
    AlignmentB = "";            % alignment B seq init;
    
    planeSize = [a, b];
    route = zeros(planeSize);   % 'optimal path' zeros matrix init; will be filled with '1' on specific F(i,j) cell where the optimal route is set;
    
    matches = 0;                % NFO DATA - matches sum init;
    mismatches = 0;             % NFO DATA - mismatches sum init;
    gaps = 0;                   % NFO DATA - gaps sum init;
    
    maximum = 0;
    minimum = 0;
    
    if mode=="similarity"           % finding the location of MAX score values in the base F matrix;
        maximum = max(max(F));
        [row,col]=find(F==maximum);
    elseif mode=="distance"
        minimum = min(min(F));
        [row,col]=find(F==minimum); % finding the location of MAX score values in the base F matrix;
    end
    
    for loop=1:1:length(row)
    route(row(loop),col(loop)) = 1; % setting the route ('0''s-'1''s) matrix initial step;
    
    i = row;                    % setting the for loop starting cell to the value of (F(end:end)); 
    j = col;                    % in order to begin the optimal routing 
                                % going: F(nxm) -> 0;
                                % 'row' and 'col' stands for the indecies
                                % of gathered MAX/MIN score values
                                % position(-s: if there is more than one 
                                % - the loop proceeds to analyse them all);
                                
    % ---------------------------------------------------------------------
    
    while (i > 1) & (j > 1) & F(i,j)~=0   % execute the loop until 'i' or 'j' from the F(a,b) reaches value '1', which is equivalent in reaching X/Y plot axis -> (F(1:b) or F(a:1)) of the matrix;
        
        mismatchScr = currentMismatchScrAssign(i, j, Achar, Bchar,  mismatchScrAT_TA, ...
mismatchScrAC_CA, mismatchScrAG_GA, mismatchScrGC_CG, mismatchScrGT_TG, mismatchScrTC_CT);       % mismatchScr variable assignment based on the current pair of nucleotide being compared;
                                                                                                 %
            matchScr = currentMatchScrAssign(i, j, Achar, Bchar, ...                             %
            matchScrAA, matchScrCC, matchScrGG, matchScrTT);                                     % matchScr -||-
        
        if Achar(i)==Bchar(j)             % init and assign the 'currentMatchScr' in order to distinguish
            currentMatchScr = matchScr;   % diagonal scoring step from match or MISmatch;
            matches = matches +1;         % NFO DATA - matches sum unitary rise;
        else
            
            currentMatchScr = mismatchScr;%
            mismatches = mismatches +1;   % NFO DATA - MISmatches sum unitary rise;
        end

        if (i > 0 & j > 0) & F(i,j) == F(i-1,j-1) + currentMatchScr & F(i,j)~=0 
                                                                      % if the current F(i,j) matrix cell value is equal to 
                                                                      % the neighbouring diagonal (upper-left) + beforehand defined match/MISmatch score 
                                                                      % go for 'synonymic/nonsynonymic SUBSTITUTION' way;
            AlignmentA = Achar(i) + AlignmentA;                       % add to A alignment current A sequence nucleotide letter;
            AlignmentB = Bchar(j) + AlignmentB;                       % add to B alignment current B sequence nucleotide letter;
            i = i - 1;                                                % go back diagonally within the F matrix;
            j = j - 1;                                                % -||-

            route(i,j)=1;                                             % set current route matrix cell value to '1';

        elseif (i > 0 & F(i,j) == F(i-1,j) + gapScr) & F(i,j)~=0      % analogous case, but this time consider INSERTION: 
                                                                      % occurs when the current F(i,j) matrix cell value is equal to the neighbouring left cell score + gap score
                                                                      % 
            AlignmentA = Achar(i) + AlignmentA;                       % add to A alignment current A sequence nucleotide letter;
            AlignmentB = "_" + AlignmentB;                            % set a GAP current B sequence nucleotide letter;
            i = i - 1;
            gaps = gaps +1;

            route(i,j)=1;

        else                                                          % analogous case, but this time consider DELETION (the only left case):
                                                                      % occurs when the current F(i,j) matrix cell value is equal to the neighbouring upper cell score + gap score
            AlignmentA = "_" + AlignmentA;                            % set a GAP current A sequence nucleotide letter;
            AlignmentB = Bchar(j) + AlignmentB;                       % add to B alignment current B sequence nucleotide letter;
            j = j - 1;
            gaps = gaps +1;

            route(i,j)=1;

        end

    end

    
    
    if i > 1    % this is the same exact INSERTION code as before,
                % having different conditions that execute the code
                % only when the upper edge of plot/matrix F(1:b) is reached,
                % but the left isn't yet;

        while (i ~= 1) & (F(i,j)~=0)
            AlignmentA = Achar(i) + AlignmentA;
            AlignmentB = "_" + AlignmentB;
            i = i - 1;
            gaps = gaps +1;

            route(i,j)=1;

        end

    end

    if (j > 1)  % this is the same exact DELETION code as before,
                % having different conditions that execute the code
                % only when the left edge of plot/matrix F(1:b) is reached,
                % but the upper isn't yet;

        while (j ~= 1) & (F(i,j) ~= 0)

            AlignmentA = "-" + AlignmentA;
            AlignmentB = Bchar(j) + AlignmentB;
            j = j - 1;
            gaps = gaps +1;

            route(i,j)=1;

        end

    end
    
    end % 'for' loop END
    
    % ---------------------------------------------------------------------
           
end