function [mismatchScr] = currentMismatchScrAssign(i, j, Achar, Bchar,  mismatchScrAT_TA, ...
mismatchScrAC_CA, mismatchScrAG_GA, mismatchScrGC_CG, mismatchScrGT_TG, mismatchScrTC_CT)
    
    % currentMismatchScrAssign() func. is responsible for currently analyzed 
    % nucleotide lineup MISMATCH score assignment inside the main algorith loop in the alignment.m function 
    % (MISmatch score set-up);
    
    mismatchScr = 0;
    
    if Achar(i)=='A' & Bchar(j)=='T' | Achar(i)=='T' & Bchar(j)=='A'
                    mismatchScr = mismatchScrAT_TA;
                elseif Achar(i)=='A' & Bchar(j)=='C' | Achar(i)=='C' & Bchar(j)=='A'
                    mismatchScr = mismatchScrAC_CA;
                elseif Achar(i)=='A' & Bchar(j)=='G' | Achar(i)=='G' & Bchar(j)=='A'
                    mismatchScr = mismatchScrAG_GA;
                elseif Achar(i)=='G' & Bchar(j)=='C' | Achar(i)=='C' & Bchar(j)=='G'
                    mismatchScr = mismatchScrGC_CG;
                elseif Achar(i)=='G' & Bchar(j)=='T' | Achar(i)=='T' & Bchar(j)=='G'
                    mismatchScr = mismatchScrGT_TG;
                elseif Achar(i)=='T' & Bchar(j)=='C' | Achar(i)=='C' & Bchar(j)=='T'
                    mismatchScr = mismatchScrTC_CT;
    
    end
end